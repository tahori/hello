{
  description = "A flake for building Hello World";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;
    bbb = {
      url = "/home/taito/hello/bbb";
      flake = false;
    };
  };
  outputs = { self, nixpkgs, bbb }: {
    packages.x86_64-linux = {
      aaa =
        with import nixpkgs { system = "x86_64-linux"; };
        stdenv.mkDerivation {
          name = "aaa";
          src = self;
          buildPhase = "echo 'AAA' > ./AAA.txt";
          installPhase = "mkdir -p $out/txt; cp ./AAA.txt $out/txt/";
        };
      bbb =
        with import nixpkgs { system = "x86_64-linux"; };
        stdenv.mkDerivation {
          name = "bbb";
          src = "${bbb}";
          installPhase = "mkdir -p $out/txt; cp ./BBB.txt $out/txt/";
        };
    };
    defaultPackage.x86_64-linux =
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        name = "hello";
        src = self;
        buildInputs = with self.packages.x86_64-linux; [ aaa bbb ];
        buildPhase = "gcc -o hello ./hello.c";
        installPhase = "mkdir -p $out/{bin,txt}; install -t $out/bin hello; cp -r ${self.packages.x86_64-linux.aaa}/txt/* $out/txt/; cp ${self.packages.x86_64-linux.bbb}/txt/* $out/txt/";
      };
  };
}
